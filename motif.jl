#!/usr/bin/env julia
include("subfunctions/motif_align.jl")
include("subfunctions/motif_echo.jl")
include("degnutil/CLI.jl")

using Fire
using .CLI

"Align multiple motifs (MMA) with restrictions to alignment length etc. Inspired by tomtom."
@main function align(io=nothing, o=nothing; length::Int=nothing, min_overlap::Int=3, min_frac::Float64=.5)
    @assert length !== nothing
    i, o = inout(io, o)
    MotifAlign.main(i, o, length; min_overlap=min_overlap, min_frac=min_frac)
end

"Echo function for inspiration (and to make sure that there are >1 functions so subfunction name actually has to be used)"
@main function echo(io=nothing, o=nothing;)
    i, o = inout(io, o)
    MotifEcho.main(i, o)
end


