#!/usr/bin/env python3
import argparse
from degnutil import argument_parsing as argue

from subfunctions import table_replace, table_2D, table_fasta, table_dense, table_excel, table_row_length

def get_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description="Collection of small scripts for table files.")
    subparsers = argue.subparser_group(parser)
    subparsers.add_parser("replace", parents=[table_replace.get_parser()], add_help=False).set_defaults(function=table_replace.main)
    subparsers.add_parser("dense", parents=[table_dense.get_parser()], add_help=False).set_defaults(function=table_dense.main)
    subparsers.add_parser("2D", parents=[table_2D.get_parser()], add_help=False).set_defaults(function=table_2D.main)
    subparsers.add_parser("fasta", parents=[table_fasta.get_parser()], add_help=False).set_defaults(function=table_fasta.main)
    subparsers.add_parser("excel", parents=[table_excel.get_parser()], add_help=False).set_defaults(function=table_excel.main)
    subparsers.add_parser("row-length", parents=[table_row_length.get_parser()], add_help=False).set_defaults(function=table_row_length.main)

    return parser

if __name__ == '__main__':
    parsed_args = get_parser().parse_args()
    parsed_args.function(parsed_args)

