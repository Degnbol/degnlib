#!/usr/bin/env zsh
# Call emboss water for all-vs-all entries between two fasta files.
# Install emboss water:
# - conda install -c bioconda emboss
# - brew install emboss
# USAGE: water.sh TARGETS.fa [OPTIONS] < QUERIES.fa > OUTFILE
# See water_score.sh and water_scores.sh for postprocess/high level call.
cat - | while read header; read seq
do
    echo "$header\n$seq" | water -auto -filter -bsequence ${=@}
done
