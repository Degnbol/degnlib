#!/usr/bin/env zsh
# install water with
# - conda install -c bioconda emboss
# - brew install emboss
# water_scores.sh seqs.fa [-gapopen 10] [-gapextend 0.5] < infile.fa > scores.tsv
echo $'seq1\tseq2\tlength\tidentity\tgaps\tscore'
water.sh ${=@} | grep -E '^# 1|^# 2|^# Length|^# Identity|^# Gaps|^# Score|^$' | sed $'s/:.* /\t/' | tr -d '# (%)' | mlr --ixtab --otsv --ips tab --ho cat
