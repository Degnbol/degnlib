#!/usr/bin/env python3
import argparse
from degnutil import argument_parsing as argue

from subfunctions import motif_meme, motif_offset, motif_mean, motif_normalize, motif_PPM2PFM, motif_tomtom, motif_align

def get_parser():
    parser = argparse.ArgumentParser(
        description="Collection of small scripts for motifs, i.e. multiple matrices described in a single file.")
    subparsers = argue.subparser_group(parser)
    subparsers.add_parser("meme", parents=[motif_meme.get_parser()], add_help=False).set_defaults(function=motif_meme.main)
    subparsers.add_parser("offset", parents=[motif_offset.get_parser()], add_help=False).set_defaults(function=motif_offset.main)
    subparsers.add_parser("mean", parents=[motif_mean.get_parser()], add_help=False).set_defaults(function=motif_mean.main)
    subparsers.add_parser("normalize", parents=[motif_normalize.get_parser()], add_help=False).set_defaults(function=motif_normalize.main)
    subparsers.add_parser("PPM2PFM", parents=[motif_PPM2PFM.get_parser()], add_help=False).set_defaults(function=motif_PPM2PFM.main)
    subparsers.add_parser("tomtom", parents=[motif_tomtom.get_parser()], add_help=False).set_defaults(function=motif_tomtom.main)
    subparsers.add_parser("align", parents=[motif_align.get_parser()], add_help=False).set_defaults(function=motif_align.main)
    
    return parser

if __name__ == '__main__':
    parsed_args = get_parser().parse_args()
    parsed_args.function(parsed_args)

