#!/usr/bin/env python3
import argparse
from degnutil import argument_parsing as argue

from subfunctions import hmmer_entropy

def get_parser():
    parser = argparse.ArgumentParser(description="Collection of small scripts for hmmer files.")
    subparsers = argue.subparser_group(parser)
    subparsers.add_parser("entropy", parents=[hmmer_entropy.get_parser()], add_help=False).set_defaults(function=hmmer_entropy.main)
    
    return parser

if __name__ == '__main__':
    parsed_args = get_parser().parse_args()
    parsed_args.function(parsed_args)

