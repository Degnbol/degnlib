#!/usr/bin/env python3
# coding=utf-8
import argparse
from pathlib import Path
from degnutil import curs_util as curs


def get_parser():
    parser = argparse.ArgumentParser(
        description="A program intended to be like the unix command less but used for viewing tables.")
    parser.add_argument("path", help="A path of a file to view.")
    return parser


def get_args():
    return get_parser().parse_args()


def main(args):
    view = curs.View(Path(args.path).open().readlines())
    view.render()
    curs.end_window()


if __name__ == '__main__':
    main(get_args())
