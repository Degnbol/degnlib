#!/usr/bin/env python3
import numpy as np
import pandas as pd
from degnlib.degnutil import array_util as ar

"""
This script is for testing if design functions are working.
"""

array00 = np.asarray([2, 3])
array01 = np.asarray([1, 0])
array02 = np.asarray([[2, -4],
                      [3, 4]])
array03 = np.asarray([[1, 1],
                      [0, 1]])
array04 = np.asarray([[ 4.,  3.,  2.,  1.,  6.,  7.,  9.,  8., 11., 12., 14., 13.],
                      [ 1.,  2.,  3.,  4.,  9.,  8.,  6.,  7., 14., 13., 11., 12.],
                      [ 1.,  1.,  1.,  1.,  2.,  2.,  2.,  2.,  3.,  3.,  3.,  3.]])
array05 = np.asarray([[1.0,.80,.90],
                      [.80,1.0,.85],
                      [.90,.85,1.0]])
array06 = np.asarray([[2, 4, 15, 20],
                      [1, 2, 3, 4],
                      [0, 0, 1, 1]])
array07 = np.asarray([[3,1,1,0],
                      [1,3,0,1],
                      [1,0,2,0],
                      [0,1,0,2]])
array08 = np.asarray([[1., 2, 3],
                      [4, 5, 6],
                      [7, 8, 9]])
array09 = np.asarray([[0.5, 0.1, 0.9],
                      [4.2, 8., -9.1]])
array00p = pd.DataFrame(array00)
array01p = pd.DataFrame(array01)
array02p = pd.DataFrame(array02)
array03p = pd.DataFrame(array03)


def test_partial_correlation01():
    # from https://www.itl.nist.gov/div898/software/dataplot/refman2/auxillar/matrpaco.htm
    m = [[42.2, 11.2, 31.9, 167.1],
         [48.6, 10.6, 13.2, 174.4],
         [42.6, 10.6, 28.7, 160.8],
         [39.0, 10.4, 26.1, 162.0],
         [34.7, 9.3, 30.1, 140.8],
         [44.5, 10.8, 8.5, 174.6],
         [39.1, 10.7, 24.3, 163.7],
         [40.1, 10.0, 18.6, 174.5],
         [45.9, 12.0, 20.4, 185.7]]
    pcor = [[1.0000, 0.4317, -0.4566, 0.1054],
            [0.4317, 1.0000, 0.6972, 0.7268],
            [-0.4566, 0.6972, 1.0000, -0.6478],
            [0.1054, 0.7268, -0.6478, 1.0000]]
    m = np.asarray(m).T
    pcor = np.asarray(pcor)

    assert np.all(np.round(ar.partial_corr(m), 4) == pcor)


def test_partial_correlation_1st():
    assert round(ar.partial_correlation_1st(array05[0, 1], array05[0, 2], array05[1, 2]), 3) == .152
    # from example in https://www.youtube.com/watch?v=3jr_vbxajcs
    cor = ar.corr(array04)
    assert np.allclose(ar.partial_correlation_1st(cor[0,1], cor[0,2], cor[1,2]), -1)


def test_partial_correlation02():
    # from example in https://www.youtube.com/watch?v=3jr_vbxajcs
    cov = ar.cov(array04)
    pcor = ar.partial_corr(array04)
    assert pcor.shape == (3, 3)
    assert np.allclose(pcor, ar._partial_corr(cov))
    # this is -1 in the actual example. 
    # There is a difference of definition of pcor it seems between the matrix method and the simple 3 variable formula.
    assert np.allclose([pcor[0,1], pcor[1,0]], 1)


def test_partial_correlation03():
    h = ar._partial_corr(array05)
    assert np.allclose(h[0,1], h[1,0])
    assert round(h[0,1], 3) == .152


def test_correlation():
    cor = ar.corr(array06)
    assert np.allclose([cor[0,1], cor[1,0]], 0.9695016)


def test_partial_correlation04():
    pcor = ar.partial_corr(array06)
    assert np.allclose([pcor[0,1], pcor[1,0]], 0.919145)
    

def test_partial_correlation05():
    # from https://rdrr.io/cran/corpcor/man/cor2pcor.html
    
    pcor = [[1.0000000, 0.4000000, 0.43852901, -0.17541160],
            [0.4000000, 1.0000000, -0.17541160, 0.43852901],
            [0.4385290, -0.1754116, 1.00000000, 0.07692308],
            [-0.1754116, 0.4385290, 0.07692308, 1.00000000]]
    
    assert np.allclose(ar._partial_corr(array07), pcor)


def test_fill_except_diag():
    a = array08.copy()
    ar.fill_except_diag(a, 3)
    assert np.all(a == np.asarray([[1, 3, 3],
                                   [3, 5, 3],
                                   [3, 3, 9]]))
    a = array08.copy()
    ar.fill_except_diag(a, array09)
    assert np.all(a == np.asarray([[1, 0.1, 0.9],
                                   [0.5, 5, -9.1],
                                   [4.2, 8., 9]]))


def test_binary():
    assert np.all(ar.binary(3) == np.asarray([1, 1], dtype=bool))
    assert np.all(ar.binary(3, 3) == np.asarray([0, 1, 1], dtype=bool))
    assert ar.binary(0, 1) == np.asarray([False])
    # chosen to be different from what we made in the string_util version
    assert len(ar.binary(0, 0)) == 1
    

def test_nonzero_mean():
    assert np.all(ar.nonzero_mean(array07, axis=1) == np.asarray([(3+1+1)/3,
                                                                  (1+3+1)/3,
                                                                  (1+2)/2,
                                                                  (1+2)/2,]))

    assert ar.nonzero_mean(array07) == (3 + 1 + 1 + 1 + 3 + 1 + 1 + 2 + 1 + 2) / (3 + 3 + 2 + 2)



def test_ndim():
    assert ar.ndim([1, 2, 3]) == 1
    assert ar.ndim(5) == 0
    assert ar.ndim("hej") == 0
    assert ar.ndim([[1, 2, 3], [4, 3, 2]]) == 2
    assert ar.ndim([5, [1, 2, 3], 2]) == 2
    assert ar.ndim((np.asarray([1, 2, 3]), np.asarray([4, 5]))) == 2

