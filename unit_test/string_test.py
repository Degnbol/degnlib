#!/usr/bin/env python3
from degnlib.degnutil import string_util as st

"""
This script is for testing if string functions are working.
"""


def test_parse_number():
    assert st.parse_number("3") == 3
    assert st.parse_number(3) == 3
    assert st.parse_number(3.5) == 3.5
    assert st.parse_number("3.1") == 3.1
    assert st.parse_number("3.0") == 3.0
    assert isinstance(st.parse_number("3.0"), float)


def test_parse_value():
    assert st.parse_value("3") == 3
    assert st.parse_value(3) == 3
    assert st.parse_value("3.1") == 3.1
    assert st.parse_value("3.0") == 3.0
    assert isinstance(st.parse_value("3.0"), float)
    assert st.parse_value("hej") == "hej"
    assert st.parse_value("4.d") == "4.d"


def test_substring():
    seq = 'ABDJSDKSHDKHDKSJHD'
    assert st.substring(seq, 3, 6, 1) == "DJSD"
    assert st.substring(seq, 3, 6, 1, 1) == "BDJSDK"
    assert st.substring(seq, 3, -2) == ""
    assert st.substring(seq, 3, -2, 0, 2) == ""
    assert st.substring(seq, -3, 7) == st.substring(seq, 0, 7)
    assert st.substring(seq, -3, 7, 0, 1) == st.substring(seq, 0, 8)


