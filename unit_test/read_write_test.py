#!/usr/bin/env python3
from degnlib.degnutil import read_write as rw

"""
This script is for testing if read/write functions are working.
"""

def test_read_vector():
    assert rw.read_vector("test_data/bg_freqs.tsv") == {'A': 0.296917, 'C': 0.143741, 'G': 0.267457, 'U': 0.291884}


