#!/usr/bin/env python3
import numpy as np
import pandas as pd
from degnlib.degnutil import pandas_util as panda

"""
This script is for testing if design functions are working.
"""

array00 = np.asarray([2, 3])
array01 = np.asarray([1, 0])
array02 = np.asarray([[2, -4],
                      [3, 4]])
array03 = np.asarray([[1, 1],
                      [0, 1]])
array04 = np.asarray([[ 4.,  3.,  2.,  1.,  6.,  7.,  9.,  8., 11., 12., 14., 13.],
                      [ 1.,  2.,  3.,  4.,  9.,  8.,  6.,  7., 14., 13., 11., 12.],
                      [ 1.,  1.,  1.,  1.,  2.,  2.,  2.,  2.,  3.,  3.,  3.,  3.]])
array05 = np.asarray([[1.0,.80,.90],
                      [.80,1.0,.85],
                      [.90,.85,1.0]])
array06 = np.asarray([[2, 4, 15, 20],
                      [1, 2, 3, 4],
                      [0, 0, 1, 1]])
array07 = np.asarray([[3,1,1,0],
                      [1,3,0,1],
                      [1,0,2,0],
                      [0,1,0,2]])
array08 = np.asarray([[1., 2, 3],
                      [4, 5, 6],
                      [7, 8, 9]])
array09 = np.asarray([[0.5, 0.1, 0.9],
                      [4.2, 8., -9.1]])
array00p = pd.DataFrame(array00)
array01p = pd.DataFrame(array01)
array02p = pd.DataFrame(array02)
array03p = pd.DataFrame(array03)


def test_sum():
    assert panda.sum(array00) == 5
    assert panda.sum(array00p) == 5


def test_std():
    assert panda.std(array00) == 0.5
    assert panda.std(pd.DataFrame(array00)) == 0.5


def test_index():
    # basic pandas test, this should not fail unless there are installation and package issues
    array00p == 2
    assert panda.sum(panda.index(array02, array03 == 1)) == 2 - 4 + 4
    assert panda.sum(panda.index(array02, array03p == 1)) == 2 - 4 + 4
    assert panda.sum(panda.index(array02p, array03 == 1)) == 2 - 4 + 4
    assert panda.sum(panda.index(array02p, array03p == 1)) == 2 - 4 + 4


