#!/usr/bin/env python3
from degnutil import collection_util as co

"""
This script is for testing if string functions are working.
"""

list00 = [3, 1, 5]
list01 = [[3], 1, 4]
list02 = [[3, 11], 1, -4]
list03 = [[3, 11], [1, -4]]

def test_unjag_list():
    co.unjag_list([])
    co.unjag_list([[]])
    co.unjag_list([0])
    co.unjag_list(list00)
    co.unjag_list(list01)
    co.unjag_list(list02)
    co.unjag_list(list03)
