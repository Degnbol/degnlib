#!/usr/bin/env python3
from pathlib import Path
from degnlib.subfunctions import fasta_motif_align

"""
This script is for testing if read/write functions are working.
"""

def test_align():
    arg_string = "test_data/small.fa test_data/small.aln.fa -m test_data/empPPMs.tsv"
    parsed_args = fasta_motif_align.get_parser().parse_args(arg_string.split())
    fasta_motif_align.main(parsed_args)
    
    assert Path("test_data/small.aln.fa").read_text() == Path("test_data/small.correct.aln.fa").read_text()


