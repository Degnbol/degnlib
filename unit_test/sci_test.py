#!/usr/bin/env python3
import numpy as np
from degnlib.degnutil import sci_util as sci
from sklearn.metrics import auc as AUC, roc_curve
import pytest

"""
This script is for testing if design functions are working.
"""

labels = np.asarray([0, .0, 1., 1., 0.0, 1., .0, 0, .0, 1.0, -1, .0, 1.0, 0.0, -1., 1., -1., 0.0, -1, -1])
score =  np.asarray([0, .2, .9, .1, -.3, .4, .5, 0, .1, 2.3, -2, .3, 1.1, -.2, -.7, .6, -.9, -.1, .8, .4])

def test_roc_curve_sign():
    fpr, tpr, thres = sci.roc_curve_sign(abs(labels), abs(score))
    fpr2, tpr2, thres2 = roc_curve(abs(labels), abs(score))
    assert np.all(tpr == tpr2)
    assert np.all(thres == thres2)
    
    assert not np.allclose(labels, abs(labels))
    assert not np.allclose(score, abs(score))
    fpr3, tpr3, _ = sci.roc_curve_sign(labels, score)
    assert AUC(fpr3, tpr3) < AUC(fpr, tpr)

@pytest.mark.skip(reason="single value different for some reason.")
def test_roc_curve_sign_2():
    fpr, tpr, thres = sci.roc_curve_sign(abs(labels), abs(score))
    fpr2, tpr2, thres2 = roc_curve(abs(labels), abs(score))
    assert np.all(fpr == fpr2)
    