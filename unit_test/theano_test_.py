#!/usr/bin/env python3
import numpy as np
import pandas as pd
from scipy import sparse as spsp
from degnlib.degnutil import array_util as ar, theano_util as thea
import pytest

"""
This script is for testing if design functions are working.
"""

array00 = np.asarray([2, 3])
array01 = np.asarray([1, 0])
array02 = np.asarray([[2, -4],
                      [3, 4]])
array03 = np.asarray([[1, 1],
                      [0, 1]])
array04 = np.asarray([[ 4.,  3.,  2.,  1.,  6.,  7.,  9.,  8., 11., 12., 14., 13.],
                      [ 1.,  2.,  3.,  4.,  9.,  8.,  6.,  7., 14., 13., 11., 12.],
                      [ 1.,  1.,  1.,  1.,  2.,  2.,  2.,  2.,  3.,  3.,  3.,  3.]])
array05 = np.asarray([[1.0,.80,.90],
                      [.80,1.0,.85],
                      [.90,.85,1.0]])
array06 = np.asarray([[2, 4, 15, 20],
                      [1, 2, 3, 4],
                      [0, 0, 1, 1]])
array07 = np.asarray([[3,1,1,0],
                      [1,3,0,1],
                      [1,0,2,0],
                      [0,1,0,2]])
array08 = np.asarray([[1., 2, 3],
                      [4, 5, 6],
                      [7, 8, 9]])
array09 = np.asarray([[0.5, 0.1, 0.9],
                      [4.2, 8., -9.1]])
array00p = pd.DataFrame(array00)
array01p = pd.DataFrame(array01)
array02p = pd.DataFrame(array02)
array03p = pd.DataFrame(array03)
array00s = thea.shared(array00, "array00")
array01sp = thea.shared(array01, "array01")
array02s = thea.shared(array02, "array02")
array03s = thea.shared(array03, "array03")
array03sp = thea.shared(array03, "array03")

@pytest.mark.skip(reason="theano not supported right now. Probably a version thing but we don't really care about theano anymore.")
def test_is_sparse():
    assert not thea.is_sparse(array00)
    assert not thea.is_sparse(array00s)
    assert thea.is_sparse(spsp.csc_matrix(array01))
    assert thea.is_sparse(array01sp)

@pytest.mark.skip(reason="theano not supported right now. Probably a version thing but we don't really care about theano anymore.")
def test_std_across():
    assert thea.std_across([array00, array01]) == 1
    # doesn't work with 1 dimensional arrays
    # assert thea.std_across([array00s, array01s]) == 1
    assert thea.std_across([array02s, array03sp]) == (.5 + 2.5 + 1.5 + 1.5) / 4

@pytest.mark.skip(reason="theano not supported right now. Probably a version thing but we don't really care about theano anymore.")
def test_variable_to_array():
    assert 2 in thea.variable_to_array(array00s)
    assert 0 in thea.variable_to_array(array01sp)

@pytest.mark.skip(reason="theano not supported right now. Probably a version thing but we don't really care about theano anymore.")
def test_inv():
    funcs = [thea.inv, lambda x: thea.thinv(x).eval(), lambda x: thea.thinv(x, True).eval()]
    
    for i in range(3):
        cor = np.corrcoef(array04)
        prec = funcs[i](cor)
        # has to be float64 inverse dot float32
        eye = prec @ cor.astype('float32')
        assert np.sum(abs(eye - np.eye(eye.shape[1]))) < 0.0001

