#!/usr/bin/env python3
from degnutil.input_output import dynamic_parser, dynamic_main

def get_parser():
    return dynamic_parser("subfunctions/fasta_*.py", "Collection of small scripts for fasta files.")

if __name__ == '__main__':
    dynamic_main(get_parser())
