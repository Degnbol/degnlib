#!/usr/bin/env zsh
# install water with conda install -c bioconda emboss
# water_score.sh seqs.fa [-gapopen 10] [-gapextend 0.5] < infile.fa > scores.tsv
water.sh ${=@} -aformat score | grep -v '^$' | grep -v '^#' | tr ' ' '\t' | tr -d '()' | cat <(echo $'seq1\tseq2\tlength\tscore') -
