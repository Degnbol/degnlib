#!/usr/bin/env python3
# coding=utf-8
import numpy as np
import argparse

import pandas as pd
from sklearn.metrics import auc as AUC, roc_curve, roc_auc_score
from matplotlib.font_manager import FontProperties
from degnutil import read_write as rw, sci_util as sci, array_util as ar, plot_util as plo, argument_parsing as argue


def get_parser():
    parser = argparse.ArgumentParser(
        description="Plot ROC curve and get AUC.")
    parser.add_argument("path", nargs="+", 
                        help="Single file with a score and classification column OR a list of tables if -p, -c or -H is given OR an even number of files "
                             "where the first files half of files have scores for different curves "
                             "and the last half the corresponding classifications.")
    parser.add_argument("-p", "--score", dest="y_score", help="Column name for prediction score. Only required for single infile with multiple float and bool columns.")
    parser.add_argument("-c", "--class", dest="y_true", help="Column name for boolean classification. Only required for single infile with multiple float and bool columns.")
    argue.delimiter_tab(parser)
    parser.add_argument("-H", "--header", action="store_true", help="Set flag to indicate the table has a header. Not needed if -p/--score or -c/--class is given.")
    parser.add_argument("-o", "--outfile", help="Save plot to file. Default is printing to screen. Set to /dev/null to not print.")
    parser.add_argument("-t", "--title", default="ROC")
    parser.add_argument("-l", "--legend", default=[''], nargs="*",
                        help="Legend text for each curve. "
                             "If only one (or none) is provided, all data will be given the same color and legend.")
    parser.add_argument("-s", "--size", default=3.5, type=float, help="Size of plot in inches.")
    parser.add_argument("-m", "--mask", nargs="*",
                        help="File with 1s and 0s on each line indicating rows to use "
                             "or 2D to indicate each entry to include in ROC.")
    parser.add_argument("-T", "--transparency", type=float, default=1,
                        help="Get transparency by setting the amount of opaqueness where 1 is opaque and 0 is transparent.")
    parser.add_argument("-S", "--small-legend", action="store_true", help="Set to reduce legend text size.")
    
    return parser
    
    
def get_args():
    args = get_parser().parse_args()
    if len(args.path) > 1:
        if len(args.legend) == 1:
            # duplicate single legend
            args.legend = [args.legend[0] for _ in range(len(args.path) // 2)]
        elif len(args.legend) != len(args.path) and len(args.legend) * 2 != len(args.path):
            raise argparse.ArgumentTypeError("Incorrect number of legend texts or paths")

    if args.y_score is not None or args.y_true is not None: args.header = True

    return args


def _read(paths, delimiter, header, y_score_col=None, y_true_col=None):
    """

    :param paths:
    :param delimiter:
    :param header:
    :param y_score_col:
    :param y_true_col:
    :return: y_trues, y_scores: which each are lists of vectors/columns
    """
    if len(paths) == 1 or header: return zip(*(read_table(p, delimiter, header, y_score_col, y_true_col) for p in paths))
    return read_paired(paths[:len(paths)//2], paths[len(paths)//2:])

def read_table(path, delimiter, header, y_score_col=None, y_true_col=None):
    """
        Read y_true, y_score from file(s).
        :param path: one path or an even number of paths where the first half are y_true and rest are y_score.
        Every pair will then be their own curve
        :param delimiter: delimiter character
        :param header: bool has header
        :param y_score_col:
        :param y_true_col:
        :return: y_trues, y_scores
        """
    df = pd.read_table(path, sep=delimiter, header=0 if header else None)

    if y_score_col is not None: y_scores = df[y_score_col]
    else:
        # see if there is no ambiguity, i.e. a single float column
        float_cols = float == df.dtypes
        if not float_cols.any():
            if header: raise argparse.ArgumentTypeError("No float column found to use as y_scores.")
            print("# Setting -H/--header = True.")
            df = pd.read_table(path, sep=delimiter)
            float_cols = float == df.dtypes
        if float_cols.sum() == 1:
            print("# Single float column found. Used as y_scores.")
            y_scores = df.loc[:, np.nonzero(float == df.dtypes)[0][0]]
        else: raise argparse.ArgumentTypeError("Ambiguity about which column contains float y_scores. Set -p/--score.")

    if y_true_col is not None: y_trues = df[y_true_col]
    else:
        if (bool == df.dtypes).sum() == 1:
            print("# Single bool column found. Used as y_trues.")
            y_trues = df.loc[:, np.nonzero(bool == df.dtypes)[0][0]]
        elif (int == df.dtypes).sum() == 1:
            print("# Single int column found. Used as y_trues.")
            y_trues = df.loc[:, np.nonzero(int == df.dtypes)[0][0]]
        else: raise argparse.ArgumentTypeError("Single bool or int column not detected for y_trues. Set -c/--class.")

    return y_trues, y_scores

def read_paired(y_score_paths, y_true_paths):
    """
    Read y_true, y_score from paired files.
    :param y_score_paths:
    :param y_true_paths:
    :return: y_trues, y_scores: which each are lists of vectors/columns
    """
    if len(y_score_paths) != len(y_true_paths):
        raise argparse.ArgumentTypeError("Only supporting 1 input file or even number of input files.")
    return ([np.asarray(rw.read_array(p)) for p in ps] for ps in (y_score_paths, y_true_paths))




def ROC_plot(fpr, tpr, legend, alpha=1, small_legend=False):
    """
    Plot ROC curves.
    :param fpr: false positive rates or list of them
    :param tpr: true positive rates or list of them
    :param legend: legend text or list of legend texts
    :param alpha:
    :param small_legend:
    :return:
    """
    if ar.ndim(fpr) == 1: fpr = [fpr]
    if ar.ndim(tpr) == 1: tpr = [tpr]
    if ar.ndim(legend) == 0: legend = [legend]

    unique, unique_index, unique_inverse = np.unique(legend, return_index=True, return_inverse=True)
    legend, colors = plo.unique_legends(legend)
    aucs = np.asarray([AUC(_fpr, _tpr) for _fpr, _tpr in zip(fpr, tpr)])
    for i in range(len(unique)):
        legend[unique_index[i]] += ' (AUC={:0.3f})'.format(aucs[unique_inverse == i].mean())

    plt.plot([0, 1], [0, 1], color='grey', linestyle='--')
    
    for _fpr, _tpr, _legend, color in zip(fpr, tpr, legend, colors):
        plt.plot(_fpr, _tpr, color=color, label=_legend, alpha=alpha)
    
    pad = 0.005
    plt.xlim([0 - pad, 1 + pad])
    plt.ylim([0 - pad, 1 + pad])
    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate')
    if not small_legend: plt.legend(loc="lower right")
    else:
        fontP = FontProperties()
        fontP.set_size('small')
        plt.legend(loc="lower right", prop=fontP)


def plot(fpr, tpr, legend="ROC curve", title="ROC", path=None, size=3.5, alpha=1, small_legend=False):
    plt.figure(figsize=(size, size))
    ROC_plot(fpr, tpr, legend, alpha, small_legend)
    plt.title(title)
    plt.tight_layout()
    if path is None: plt.show()
    else:
        plt.savefig(path)
        plt.clf()


def y_true_y_score(y_true, y_score):
    """
    Make sure they are of correct format.
    :param y_true: 
    :param y_score: 
    :return: 
    """
    return np.asarray(np.sign(y_true)).ravel(), np.asarray(y_score).ravel()


def ROCs(y_true, y_score):
    """
    Return either simple ROC or ROC for each class plus average ROC as macro and micro.
    :param y_true: boolean or signed
    :param y_score: float scores
    :return: fpr, tpr, each as lists of 1Ds
    """
    y_true, y_score = y_true_y_score(y_true, y_score)
    if not np.any(y_true < 0): return zip(*[roc_curve(y_true, y_score)[:2]])
    return zip(*[roc_curve(y_true < 0, -y_score)[:2],
                 roc_curve(y_true > 0, y_score)[:2],
                 # sci.roc_macro(y_true, y_score)[:2],
                 # sci.roc_micro(y_true, y_score)[:2],
                 roc_curve(abs(y_true), abs(y_score))[:2],])


def ROCAUC(y_true, y_score):
    """
    Get ROC AUC possibly with signed values indicating class.
    :param y_true: array or table
    :param y_score: array or table
    :return: int AUC
    """
    y_true, y_score = y_true_y_score(y_true, y_score)
    if not np.any(y_true < 0): return roc_auc_score(y_true, y_score)
    return AUC(*sci.roc_macro(y_true, y_score))


def mask(arrays, masks):
    """
    mask rows in arrays
    :param arrays: 
    :param masks: 
    :return: 
    """
    # if one mask provided, it is used for all
    if len(masks) == 1: masks = [masks[0] for _ in arrays]
    return [a[m == 1] for a, m in zip(arrays, masks)]


def main(args):
    y_trues, y_scores = _read(args.path, args.delimiter, args.header, args.y_score, args.y_true)
    
    # mask rows if mask(s) given
    if args.mask is not None:
        masks = [np.asarray(rw.read_array(p)) for p in args.mask]
        y_trues = mask(y_trues, masks)
        y_scores = mask(y_scores, masks)
    
    # flatten
    y_trues = [a.ravel() for a in y_trues]
    y_scores = [a.ravel() for a in y_scores]
    
    fprs, tprs, legends = [], [], []
    for y_true, y_score, legend in zip(y_trues, y_scores, args.legend):
        fpr, tpr = ROCs(y_true, y_score)
        fprs += fpr
        tprs += tpr
        if len(fpr) == 1: legends.append(legend)
        else: legends += [(legend + " " + l).strip() for l in ["neg", "pos", "abs"]]
        
    for fpr, tpr in zip(fprs, tprs): print(AUC(fpr, tpr))
    if args.outfile != "/dev/null":
        plot(fprs, tprs, legends, args.title, args.outfile, args.size, args.transparency, args.small_legend)


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    main(get_args())




