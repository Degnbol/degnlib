#!/usr/bin/env python3
# An attempt at extending the open command for the xgmml format.
# Opens the file in cytoscape.
# REQUIRES:
# - py4cytoscape installed, e.g. with pip
# - cyREST installed in cytoscape app manager.
# - cytoscape running to be controlled. 
# USE: ./open-xgmml INFILE.xgmml
import sys
import time
import py4cytoscape as p4c

for attempt in range(50):
    try:
        p4c.cytoscape_ping()
    except:
        time.sleep(5)
        continue
    else:
        break
    print("Cytoscape not ready")
    exit(1)

for infile in sys.argv[1:]:
    p4c.import_network_from_file(infile)
