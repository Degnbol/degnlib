#!/usr/bin/env python3
# coding=utf-8
import os
import argparse
from pathlib import Path
import time
from degnutil import input_output as io, argument_parsing as argue, path_util as pa, string_util as st


# constants
account = "cu_10043"


def get_parser():
    parser = argparse.ArgumentParser(description="A general computerome submitter script.")
    parser.add_argument("command", help="A string indicating the code to run.")
    parser.add_argument("-N", "--name", help="Name of the submission job.")
    parser.add_argument("-minutes", "--minutes", dest="minutes", type=int, default=0,
                        help="Wall-time minutes. Default is 0 unless no kind of wall-time is provided, then it is 30.")
    parser.add_argument("-hours", "--hours", type=int, default=0, help="Wall-time hours. Default is 0.")
    parser.add_argument("-mem", "--memory", type=int, help="Memory in gb. Default is 20.", default=20)
    parser.add_argument("-dir", "--workdir", default=os.getcwd(), 
                        help="The submission is run from here and files are saved here. "
                             "Relative paths in command is relative to this. "
                             "Default is current directory.")
    parser.add_argument("-wait", "--wait-for", type=int, 
                        help="Provide a job ID for another job to wait for its successful completion.")
    parser.add_argument("-rep", "--repeat", type=int, 
                        help="Create a job array by setting a number of times the job should be repeated. "
                             "The variable $PBS_ARRAYID will be different from each job and run from 0 to the given value minus 1.")
    parser.add_argument("-max", "--max-jobs", type=int, default=10, 
                        help="Maximum number of this job running in parallel. "
                             "Only applicable for array jobs set with -rep/--repeat argument.")
    argue.force(parser, help="By default file names are incremented, set this to overwrite.")
    argue.quiet(parser)
    argue.verbose(parser)
    argue.nproc(parser)
    return parser
    
    
def get_args(args=None):
    # parse args either given as string or list
    if isinstance(args, str): args = argue.parse_args(get_parser(), args)
    else: args = get_parser().parse_args(args)
    
    if not args.force: args.name = increment_name(args.workdir, args.name)
    io.log.info("job name set to {}".format(args.name))
    if pa.any_exists(get_submission_paths(args.workdir, args.name)):
        io.log.warning("submission files will be overwritten")

    # if all time args are zero
    if not args.minutes and not args.hours: args.minutes = 30
    return args


def get_submission_paths(workdir, name=""):
    """
    Get paths of .qsub, .out, .err files
    :param workdir: path to folder where they will be stored
    :param name: name of job
    :return: list of three paths
    """
    root = workdir + "/sub"
    if name: root += "_" + name
    return [root + suffix for suffix in ['.qsub', '.out', '.err']]


def increment_name(workdir, name=""):
    """
    Get a job name where we by default look for name conflict and increment names.
    :param workdir: working directory where we save the file
    :param name: unincremented name
    :return: job name incremented
    """
    paths = pa.increment(get_submission_paths(workdir, name))
    # get the name part of one of the resulting filename
    return Path(paths[0]).stem[len("sub_"):]


qsub_lines = [
    '#!/bin/sh',
    '#PBS -W group_list={account} -A {account}',
    '#PBS -N {name}',
    '#PBS -e {workdir}/sub_{name}.err',
    '#PBS -o {workdir}/sub_{name}.out',
    '#PBS -M n',
    '#PBS -r y',
    '#PBS -l nodes=1:ppn={nproc}',
    '#PBS -l mem={memory}gb',
    '#PBS -l walltime={walltime}',
    'echo "Working directory is $PBS_O_WORKDIR"',
    'cd $PBS_O_WORKDIR',
    'export NPROC=`wc -l < $PBS_NODEFILE`',
    'echo "This job has allocated $NPROC nodes"',
    'echo "Started on `date`"',
    'source ~/.bash_profile',
    '{command}']


def get_qsub(lines, **kwargs):
    """
    Format the qsub string and be flexible about the values given.
    :param lines: list of strings with {keyword} notations for the string formatter to recognize
    :param kwargs: keywords that can be used to format the qsub string
    :return: qsub string
    """
    if "walltime" not in kwargs: kwargs["walltime"] = st.time_to_string(**kwargs)
    # format works with Path
    kwargs["workdir"] = Path(kwargs["workdir"]).absolute() if "workdir" in kwargs else Path.cwd()
    
    return "\n".join(lines).format(**kwargs) + "\n"


def submit(path):
    """
    Submit a qsub file from the folder it is positioned in and provide a small delay after
    :param path: path to .qsub file
    :return: None
    """
    original_dir = os.getcwd()
    path = Path(path)
    abspath = path.absolute()
    os.chdir(path.parent)
    os.system('qsub {}'.format(abspath))
    os.chdir(original_dir)
    time.sleep(2)


def main():
    args = get_args()
    io.log.info("args: {}".format(str(args)))
    
    lines = qsub_lines[:2]
    if args.wait_for: lines += ['#PBS -W depend=afterok:{wait_for}']
    if args.repeat:
        args.repeat -= 1  # make sure to run to 1 less than the specified number of repeats
        lines += ['#PBS -t 0-{repeat}%{max_jobs}']
    lines += qsub_lines[2:]
    
    qsub_path, out_path, err_path = get_submission_paths(args.workdir, args.name)
    qsub_string = get_qsub(lines, **vars(args), account=account)
    io.log.info("write qsub {}".format(qsub_path))
    Path(qsub_path).write_text(qsub_string)
    io.log.info("submit qsub")
    submit(qsub_path)
    io.log.info("submitted")

