#!/usr/bin/env python3
# coding=utf-8
import os, sys

# cancel all jobs found in stdin as the first 8 characters in a line.
# they have to be separated from other text by . or whitespace.
# they are canceled individually so it does not matter if all given are still active

for line in sys.stdin:
    if len(line) != 8 and line[8] not in '. [(\t\n': continue
    l0 = line[:8]
    try: int(l0)
    except ValueError: continue
    else: os.system('canceljob ' + l0)


