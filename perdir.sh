#!/usr/bin/env bash
# perform code from inside multiple dirs.
# the path variable $dir will be available.
# example with correct quoting use:
# ./perdir.sh '*/' 'ls *'
start_dir=$PWD
for dir in $1; do
echo "$dir"
cd "$dir"
eval $2
cd "$start_dir"
done
