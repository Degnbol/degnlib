#!/usr/bin/env python3
# coding=utf-8
import argparse
from degnutil import curs_util as curs, read_write as rw


def get_parser():
    parser = argparse.ArgumentParser(
        description="A program intended to be like the unix command less but used for viewing tables.")
    parser.add_argument("path", help="A path of a file to view.")
    parser.add_argument("-c", "--color", action="store_true", help="Color the cells based on their value.")
    parser.add_argument("-N", "--line-number", action="store_true", help="Write line numbers as first column.")
    return parser
    
    
def get_args():
    return get_parser().parse_args()


def max_cell_widths(table):
    """
    Get maximum cell widths based on index and column names for pandas and max across all values for numpy array.
    :param table: 
    :return: 
    """
    try: return [max(len(str(idx)) for idx in table.index)] + [len(c) for c in table.columns]
    except AttributeError: return [max(len(str(v)) for v in table[:,c]) for c in range(table.shape[1])]


def main(args):
    table = rw.read_array(args.path)
    view = curs.ViewTable(table, max_cell_widths(table))
    view.render()
    del view


if __name__ == '__main__':
    main(get_args())
