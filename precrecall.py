#!/usr/bin/env python3
# coding=utf-8
import numpy as np
import argparse
from sklearn.metrics import precision_recall_curve, auc as AUC
from rocauc import read, y_true_y_score
from degnutil import array_util as ar, plot_util as plo


def get_parser():
    parser = argparse.ArgumentParser(
        description="Plot precision-recall curve and get average precision-recall score.")
    parser.add_argument("path", nargs="+", 
                        help="Single file with column y_true and y_score or an even number of files "
                             "where the first half of files have y_true for different curves "
                             "and the last half the corresponding y_scores.")
    parser.add_argument("-out", "--outfile", help="Save plot to file (default is printing to screen).")
    parser.add_argument("-title", "--title")
    parser.add_argument("-leg", "--legend", default=[''], nargs="*", 
                        help="Legend text for each curve. "
                             "If legend text are the same for some curves they are given the same color and a single shared legend.")
    parser.add_argument("-size", "--size", type=int, nargs="+", default=[5, 5], help="Size of figure in inches.")
    return parser
    
    
def get_args():
    args = get_parser().parse_args()
    if len(args.size) == 1: args.size = [args.size, args.size]
    elif len(args.size) > 2: 
        raise argparse.ArgumentTypeError("Only 1 or 2 values accepted for -size/--size.")
    return args


def precision_recall_plot(prec, recall, legend):
    """
    Plot ROC curves.
    :param prec: false positive rates or list of them
    :param recall: true positive rates or list of them
    :param legend: legend text or list of legend texts.
    If legend text are the same for some curves they are given the same color and a single shared legend
    :return: 
    """
    if ar.ndim(prec) == 1: prec = [prec]
    if ar.ndim(recall) == 1: recall = [recall]
    if ar.ndim(legend) == 0: legend = [legend]

    legend, colors = plo.unique_legends(legend)
    
    for _prec, _recall, _legend, color in zip(prec, recall, legend, colors):
        plt.step(_recall, _prec, color=color, label=_legend)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.005])
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.legend(loc="upper right")


def plot(prec, recall, legend, title, path=None, size=(5, 5)):
    plt.figure(figsize=size)
    precision_recall_plot(prec, recall, legend)
    if title is not None: plt.title(title)
    if path is None: plt.show()
    else:
        plt.savefig(path)
        plt.clf()


def precision_recalls(y_true, y_score):
    """
    Return either simple precision-recall or precision-recall for each class plus average ROC as macro and micro.
    :param y_true: boolean or signed
    :param y_score: float scores
    :return: prec, recall, each as lists of 1Ds
    """
    y_true, y_score = y_true_y_score(y_true, y_score)
    if not np.any(y_true < 0): return zip(*[precision_recall_curve(y_true, y_score)[:2]])
    return zip(*[precision_recall_curve(y_true < 0, -y_score)[:2],
                 precision_recall_curve(y_true > 0, y_score)[:2],
                 # sci.precision_recall_macro(y_true, y_score)[:2],
                 # sci.precision_recall_micro(y_true, y_score)[:2],
                 precision_recall_curve(abs(y_true), abs(y_score))[:2],])


def main(args):
    y_trues, y_scores = read(args.path)
    
    precs, recalls, legends = [], [], []
    for y_true, y_score, legend in zip(y_trues, y_scores, args.legend):
        prec, recall = precision_recalls(y_true, y_score)
        precs += prec
        recalls += recall
        if len(prec) == 1: legends.append(legend)
        else: legends += [legend + " " + l for l in ["neg", "pos", "abs"]]

    plot(precs, recalls, legends, args.title, args.outfile, args.size)
    for prec, recall in zip(precs, recalls): print(AUC(recall, prec))


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    main(get_args())




