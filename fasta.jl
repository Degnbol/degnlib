#!/usr/bin/env julia
include("degnutil/ArgParseUtil.jl")

using ArgParse
using .ArgParseUtil

function argument_settings()
    settings = ArgParseSettings()

    @add_arg_table! settings begin
        "IC"
            help = "Get information content for sequences. Can be found ungrouped, grouped by sequence ids, or with a custom grouping."
            action = :command
    end
    
    @add_arg_table! settings["IC"] begin
        "io"
            default = nothing
        "o"
            default = nothing
        "-a", "--alphabet"
            default = ""
            help = "Use an alphabet, e.g. ACGU. Default is alphabet equal to the unique characters found in the input sequences."
        "-g", "--group"
            nargs = '?'
            constant = true
            default = false
        "-d", "--delimiter"
            default = isspace
        "-H", "--header"
            nargs = 0
    end

    settings
end


command, (positionals, optionals) = parse_arguments(argument_settings())
@info("Command: $command")
@info("Positionals: $positionals")
@info("Optionals: $optionals")
include("subfunctions/fasta_" * replace(command, '-'=>'_') * ".jl")  # filenames use '_' instead of '-'
main(positionals...; optionals...)  # main must be defined in the included file.

