#!/usr/bin/env zsh
# Print length of each line from stdin to stdout
# USAGE
# lengths.sh < infile > outfile or cat infile | lengths.sh > outfile
while IFS= read -r line; do
  echo ${#line}
done
