#!/usr/bin/env python3
import argparse
from degnutil import argument_parsing as argue

from subfunctions import alignment_consensus_location, alignment_squeeze

def get_parser():
    parser = argparse.ArgumentParser(description="Collection of small scripts for alignment purposes.")
    subparsers = argue.subparser_group(parser)
    subparsers.add_parser("consensus-location", parents=[alignment_consensus_location.get_parser()], add_help=False).set_defaults(function=alignment_consensus_location.main)
    subparsers.add_parser("squeeze", parents=[alignment_squeeze.get_parser()], add_help=False).set_defaults(function=alignment_squeeze.main)
    
    return parser

if __name__ == '__main__':
    parsed_args = get_parser().parse_args()
    parsed_args.function(parsed_args)

