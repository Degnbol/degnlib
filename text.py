#!/usr/bin/env python3
import argparse
from degnutil import argument_parsing as argue

from subfunctions import text_grep

def get_parser():
    parser = argparse.ArgumentParser(
        description="Collection of small scripts for general text files.")
    subparsers = argue.subparser_group(parser)
    subparsers.add_parser("grep", parents=[text_grep.get_parser()], add_help=False).set_defaults(function=text_grep.main)
    
    return parser

if __name__ == '__main__':
    parsed_args = get_parser().parse_args()
    parsed_args.function(parsed_args)

