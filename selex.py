#!/usr/bin/env python3
import argparse
from degnutil import argument_parsing as argue

from subfunctions import selex_table, selex_delete_insertions

def get_parser():
    parser = argparse.ArgumentParser(description="Collection of small scripts for table files.")
    subparsers = argue.subparser_group(parser)
    subparsers.add_parser("table", parents=[selex_table.get_parser()], add_help=False).set_defaults(function=selex_table.main)
    subparsers.add_parser("delete-insertions", parents=[selex_delete_insertions.get_parser()], add_help=False).set_defaults(function=selex_delete_insertions.main)
    
    return parser

if __name__ == '__main__':
    parsed_args = get_parser().parse_args()
    parsed_args.function(parsed_args)

