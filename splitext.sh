#!/usr/bin/env bash
# example use:
# read root ext <<< $(splitext.sh "hej/med/dig.fa")
extension="${1##*.}"
root="${1%.*}"
echo $root $extension
