#!/usr/bin/env zsh
# install prereqs
sudo cpanm Locale::Recode
sudo cpanm Unicode::Map
sudo cpanm Spreadsheet::ParseExcel
sudo cpanm Text::CSV_XS
