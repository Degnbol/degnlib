#!/usr/bin/env python3
# coding=utf-8
import sys
from os.path import isfile
import re
import numpy as np
import argparse
import pandas as pd
from sklearn.metrics import auc as AUC, roc_auc_score as auc, roc_curve as ROC, confusion_matrix
from degnutil import argument_parsing as argue, input_output as io

def get_parser():
    parser = argparse.ArgumentParser(description="""Calculate AUC. Intended to 
            work as a mlr stats2 function. Table given as stdin or first arg. 
            Writes table to stdout.""")
    parser.add_argument("infiles", nargs="*", help="Table with at least a score and class column. Multiple infiles will be concatenated.")
    parser.add_argument("-c", "--class", dest="class_col", default="label", help="Column name for classification. Either two classes, or provide -t/--thres or -m/--multi.")
    parser.add_argument("-p", "--score", nargs='+', dest="score_cols", default=["pred"], help="Column name(s) for prediction score(s).")
    parser.add_argument("-g", "--group", help="Group by this column.")
    parser.add_argument("-G", "--group2", action="store_true", help="Scoring thresholds relevant to -b/--best, -a/--accuracy and -C/--confusion are NOT calculated per -g/--group by default, set this flag to do so.")
    parser.add_argument("-t", "--thres", nargs="+", type=float, help="Threshold(s) if class is ordinal, e.g. 0.5 for a float or 50 if percentage.")
    parser.add_argument("-m", "--multi", nargs='+', choices=["ovr", "ovo", "weight_ovr", "weight_ovo", "abs", "pos", "neg"],
            help="""How to deal with multi-class.
            (ovr) take macro-average one-vs-rest.
            (ovo) take macro-average one-vs-one.
            (weight_ovr) take weighted average one-vs-rest.
            (weight_ovo) take weighted average one-vs-one.
            (Abs) take absolute of class.
            (Pos) take >0 of class. 
            (Neg) take <0 of class and invert score.""")
    parser.add_argument("-n", "--count", action="store_true", help="Count number of records for each class.")
    parser.add_argument("-b", "--best", nargs="*", default=None,
            help="""Add column with the best scoring threshold, according to choice(s) in definition:
            J/Youden, acc(uracy), as flag for default (J), or a custom expr of TN, FP, FN, TP, e.g. "TP-FP".
            Case-insensitive. Not implemented for multi-class. Also see -a and -G.""")
    parser.add_argument("-a", "--accuracy", nargs="*", help="Add columns with accuracy for given scoring threshold(s), as numeric value, J/Youden, acc(uracy) or as flag to use thresholds from -b/--best.")
    parser.add_argument("-C", "--confusion", nargs="*", help="Add columns with confusion for given scoring threshold(s), as numeric value, J/Youden, acc(uracy) or as flag to use thresholds from -b/--best.")
    argue.delimiter_tab(parser)
    
    styles = parser.add_argument_group("Header styling",
            """Default naming examples: "class score AUC", "class score AUC_macro", "class 0"...""")
    styles.add_argument("-D", "--header-sep", default="_", help="Separator between text elements in output column naming.")
    styles.add_argument("-H", "--header-minimal", action="store_true", help="Don't write class or score names to header, except if there are multiple.")
    styles.add_argument("-T", "--header-thres", default="thres", help="String to represent threshold.")
    styles.add_argument("-A", "--header-acc", default="acc", help="String to represent accuracy.")
    
    return parser

def accuracy(y_true, y_score, threshold):
    "Get a scalar float accuracy and allow nan to pass through."
    if np.isnan(threshold): return np.nan
    return np.mean((y_score > threshold) == y_true)

def confusion(y_true, y_score, threshold):
    "Get a vector with the four cells from a confusion matrix (binary case): TN, FP, FN, TP."
    y_score = y_score > threshold
    # handle edge case where only one label is present
    labels = list(np.unique([y_true, y_score]))
    if len(labels) == 1: labels += [np.nan]
    else: labels = None
    return confusion_matrix(y_true, y_score, labels=labels).ravel()

# extended support to other terms:
# https://en.wikipedia.org/wiki/Sensitivity_and_specificity
confusTerms = dict(
        P="TP+FP",
        N="TN+FN",
        TPR="TP/(TP+FN)",
        TNR="TN/(TN+FP)",
        PPV="TP/(TP+FP)",
        NPV="TN/(TN+FN)",
        FNR="FN/(FN+TP)",
        FPR="FP/(FP+TN)",
        FDR="FP/(FP+TP)",
        FOR="FN/(FN+TN)",
        TS="TP/(TP+FN+FP)",
        ACC="(TP+TN)/(TP+TN+FP+FN)",
        F1="2*TP/(2*TP+FP+FN)",
        J="TP/(TP+FN)-FP/(FP+TN)") # TPR - FPR
confusTerms["SENS"] = confusTerms["SENSITIVITY"] = confusTerms["RECALL"] = confusTerms["TPR"]
confusTerms["SPEC"] = confusTerms["SPECIFICITY"] = confusTerms["TNR"]
confusTerms["PREC"] = confusTerms["PRECISION"] = confusTerms["PPV"]
# add parens so math will work in all cases.
confusTerms = {k: f"({v})" for k, v in confusTerms.items()}

def confusionExpr(string):
    """
    Get a function(TN, FP, FN, TP) given an expression with these terms or 
    any terms from confusTerms.
    """
    # \w+ also matches numbers, so F1 is matched and numbers are passed through 
    # since there is no entry in the dict.
    return re.sub('\w+', lambda m: confusTerms.get(m.group().upper(), m.group()), string)

def confusionEval(y_true, y_score, expr):
    """
    Calculate a custom expression that is a function of TN, FP, FN, TP.
    -expr: string expression, e.g. "TP-FP".
    """
    expr = confusionExpr(expr)
    f = lambda TN, FP, FN, TP: eval(expr)
    _, _, thresholds = ROC(y_true, y_score)
    confus = [confusion(y_true, y_score, thres) for thres in thresholds]
    return [f(*c) for c in confus], thresholds

def Youden(y_true, y_score):
    """
    Calculate Youden's J statistic for each threshold along a ROC curve.
    :return: Js, thresholds
    """
    fpr, tpr, thresholds = ROC(y_true, y_score)
    return tpr - fpr, thresholds

def accuracies(y_true, y_score):
    """
    Calculate accuracy for each threshold along a ROC curve.
    :return: accuracies, thresholds
    """
    _, _, thresholds = ROC(y_true, y_score)
    accs = [accuracy(y_true, y_score, thres) for thres in thresholds]
    return accs, thresholds

def maxYoudenThres(y_true, y_score):
    """
    Get the threshold for which Youden's J statistic is maximized.
    :return: int threshold
    """
    Js, thresholds = Youden(y_true, y_score)
    return thresholds[np.argmax(Js)]

def maxAccThres(y_true, y_score):
    """
    Get the threshold for which accuracy is maximized.
    :return: int threshold
    """
    accs, thresholds = accuracies(y_true, y_score)
    return thresholds[np.argmax(accs)]

def maxConfusionThres(y_true, y_score, expr):
    """
    Get the threshold that results in the maximum of an expression,
    which is a function of some combination of TN, FP, FN, and TP.
    -expr: function, e.g. lambda TN, FP, FN, TP: ...
    """
    metrics, thresholds = confusionEval(y_true, y_score, expr)
    return thresholds[np.argmax(metrics)]
    
    

name2func = dict(
AUC            = lambda t, s: auc(t, s),
AUC_ovr        = lambda t, s: auc(t, s, multi_class="ovr"),
AUC_ovo        = lambda t, s: auc(t, s, multi_class="ovo"),
AUC_weight_ovr = lambda t, s: auc(t, s, multi_class="ovr", average="weight"),
AUC_weight_ovo = lambda t, s: auc(t, s, multi_class="ovo", average="weight"),
AUC_abs        = lambda t, s: auc(abs(t), abs(s)),
AUC_pos        = lambda t, s: auc(t > 0, s),
AUC_neg        = lambda t, s: auc(t < 0, -s),
J              = maxYoudenThres,
)

def nanfunc(func_name, y_true, y_score):
    try: return name2func[func_name](y_true, y_score)
    except ValueError: return np.nan


def parseFuncArg(arg, default=["J"]):
    """
    For interpreting function name(s) given in -b, -a and -C.
    """
    if arg is None: return None
    elif arg == []: return default # assuming default is found in name2func
    for func_name in arg:
        if func_name.capitalize() in ["J", "Youden"]:
            # allow the style given in arg
            name2func[func_name] = maxYoudenThres
        elif func_name.lower() in ["acc", "accuracy"]:
            # allow the style given in arg
            name2func[func_name] = maxAccThres
        else:
            # assume expression of TN, FP, FN and/or TP.
            # If it was a typo there will be some error from unrecognized variable when called.
            name2func[func_name] = lambda t, s: maxConfusionThres(t, s, func_name)
    return arg

def main(args):
    # check args
    if args.group2 and not args.group:
        args.group2 = False
        sys.stderr.write("-G/--group-best ignored. Only meaningful given -g/--group.\n")
    
    # check if files have been placed after args (by mistake) and move them. Can only happen for nargs=+ string args.
    score_cols = []
    for s in args.score_cols:
        if isfile(s): args.infiles.append(s)
        else: score_cols.append(s)
    
    # read
    dfs = [pd.read_table(infile, sep=args.delimiter) for infile in args.infiles]
    if io.is_reading_from_pipe(): dfs = [pd.read_table(sys.stdin, sep=args.delimiter)] + dfs
    df = pd.concat(dfs, ignore_index=True)
    
    # check data and data dependent args
    n_classes = len(df[args.class_col].unique())
    if n_classes == 1:
        raise argparse.ArgumentTypeError(f"Only one class in \"{args.class_col}\".")
    elif n_classes == 2 or args.thres is not None:
        n_classes = 2 # for the check in args.best.
        func_names = ["AUC"]
    elif args.multi:
        func_names = ["AUC_"+m for m in args.multi]
    else:
        raise argparse.ArgumentTypeError("-t/--thres or -m/--multi must be given if not boolean classes.")
    
    # header styling
    nameT = args.header_thres
    
    def nameC(c):
        if args.header_minimal:
            return c.removeprefix(class_col).lstrip(args.header_sep)
        return c
    
    def nameS(s):
        if args.header_minimal and len(score_cols) == 1:
            return ""
        return s
    
    def join(*vs):
        "Join function for the column names."
        # allow numbers
        vs = map(str, vs)
        # ignore empty string (but None and 0 would be printed)
        return args.header_sep.join([v for v in vs if v])
    
    # interpreting the -b/--best arg
    if args.best is not None:
        if n_classes != 2:
            args.best = None
            sys.stderr.write("-b/--best ignored. Not implemented for multi-class.\n")
        else:
            args.best = parseFuncArg(args.best)
    args.accuracy  = parseFuncArg(args.accuracy,  args.best if args.best else ["J"])
    args.confusion = parseFuncArg(args.confusion, args.best if args.best else ["J"])
    
    # potentially make thresholded columns
    if args.thres:
        class_cols = []
        for thres in args.thres:
            if int(thres) == thres: thres = int(thres)
            class_col = join(args.class_col, nameT, thres)
            class_cols.append(class_col)
            df[class_col] = (df[args.class_col] > thres).astype(int)
    else:
         class_cols = [args.class_col]   
    
    if args.group: dfg = df.groupby(args.group)
    def apply(grouped, f):
        return dfg.apply(f) if grouped else f(df)
    
    out = {}
    
    for class_col in class_cols:
        if args.count:
            for klass in set(df[class_col]):
                f = lambda g: sum(g[class_col] == klass)
                out[join(nameC(class_col), klass)] = apply(args.group, f)
        
        for score_col in score_cols:
            for func_name in func_names:
                f = lambda g: nanfunc(func_name, g[class_col], g[score_col])
                out[join(nameC(class_col), nameS(score_col), func_name)] = apply(args.group, f)
            
            if args.best:
                for func_name in args.best:
                    f = lambda g: nanfunc(func_name, g[class_col], g[score_col])
                    out[join(nameC(class_col), nameS(score_col), func_name, nameT)] = apply(args.group2, f)
            
            if args.accuracy:
                for func_name in args.accuracy:
                    # NOTE how we consider both args.group and args.group2.
                    # args.group decides if we are calculating the accuracy on each group,
                    # and args.group2 decides if we are calculating the scoring threshold on each group.
                    try:
                        score_thres = float(func_name)
                    except ValueError:
                        if not args.group2:
                            score_thres = nanfunc(func_name, df[class_col], df[score_col])
                            f = lambda g: accuracy(g[class_col], g[score_col], score_thres)
                        else:
                            f = lambda g: accuracy(g[class_col], g[score_col],
                                    nanfunc(func_name, g[class_col], g[score_col]))
                    else:
                        f = lambda g: accuracy(g[class_col], g[score_col], score_thres)
                    out[join(nameC(class_col), nameS(score_col), func_name, nameT, args.header_acc)] = apply(args.group, f)
            
            if args.confusion:
                for func_name in args.confusion:
                    # NOTE how we consider both args.group and args.group2.
                    # args.group decides if we are calculating the confusion on each group,
                    # and args.group2 decides if we are calculating the scoring threshold on each group.
                    try:
                        score_thres = float(func_name)
                    except ValueError:
                        if not args.group2:
                            score_thres = nanfunc(func_name, df[class_col], df[score_col])
                            f = lambda g: confusion(g[class_col], g[score_col], score_thres)
                        else:
                            f = lambda g: confusion(g[class_col], g[score_col],
                                    nanfunc(func_name, g[class_col], g[score_col]))
                    else:
                        f = lambda g: confusion(g[class_col], g[score_col], score_thres)
                    confus = apply(args.group, f)
                    # if grouped, it will be a series where each element is a list,
                    # so we reshape to combine the nth element of each list.
                    # If we were to built a df instead of a dict,
                    # then the same result can be done with
                    # out[new_cols] = pd.DataFrame.from_records(confus)
                    if args.group: confus = zip(*confus)
                    new_cols = [join(nameC(class_col), nameS(score_col), func_name, cell) for cell in ["TN", "FP", "FN", "TP"]]
                    out.update(zip(new_cols, confus))
    
    if args.group:
        out = pd.DataFrame(out).reset_index()
    else:
        # when all elements are scalar, pandas requires an index specified
        out = pd.DataFrame(out, index=[0])
    
    out.to_csv(sys.stdout, sep=args.delimiter, index=False)

if __name__ == '__main__':
    main(get_parser().parse_args())

debug = False
if debug:
    args = get_parser().parse_args("~/GT/results/10-experimentalValidation/blosum62Amb-pred.tsv -p pred -c Yield -t 50 -b TP-FP -C -a -g enzyme -H".split(' '))
    main(args)
    
