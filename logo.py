#!/usr/bin/env python3
import sys
import argparse
from degnutil import argument_parsing as argue
from pathlib import Path
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import logomaker

# default alphabet form hmmer3
alphabet = "ACDEFGHIKLMNPQRSTVWY"

def get_parser():
    parser = argparse.ArgumentParser(description="Draw sequence logo.")
    argue.infiles(parser)
    parser.add_argument("-o", "--out", dest="outfile", help="Output name of plot.")
    parser.add_argument("--step", dest="xtick_step", help="Step size between each tick mark on x-axis. "
                                                          "Default is adaptive depending on sequence length.")
    return parser


def plot_logo(mat, ax, xtick_step=None):
    logomaker.Logo(mat, ax=ax, show_spines=False)

    if xtick_step is None:
        if len(mat) >= 100: xtick_step = 25
        elif len(mat) > 20: xtick_step = 10
        elif len(mat) > 10: xtick_step = 5
        else: xtick_step = 1
    
    # one-indexed visualization
    ax.set_xticks([0] + list(range(xtick_step-1, len(mat), xtick_step)))
    ax.set_xticklabels([1] + list(range(xtick_step, len(mat)+1, xtick_step)))


def main(args):
    mats = [pd.DataFrame(np.loadtxt(infile, dtype=float), columns=list(alphabet)) for infile in args.infiles]
    width = 0.1 * max(len(mat) for mat in mats)
    height = n = len(mats)
    fig, ax = plt.subplots(n, 1, figsize=[width, height])
    if n == 1:
        plot_logo(mats[0], ax, args.xtick_step)
    else:
        for i in range(n):
            plot_logo(mats[i], ax[i], args.xtick_step)
    
    fig.tight_layout()
    if args.outfile is None: plt.show()
    else: plt.savefig(args.outfile)


if __name__ == '__main__':
    parsed_args = get_parser().parse_args()
    main(parsed_args)

